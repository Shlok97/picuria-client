import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photographer-profile-page',
  templateUrl: './photographer-profile-page.component.html',
  styleUrls: ['./photographer-profile-page.component.css']
})
export class PhotographerProfilePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
