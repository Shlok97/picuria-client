import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerProfilePageComponent } from './photographer-profile-page.component';

describe('PhotographerProfilePageComponent', () => {
  let component: PhotographerProfilePageComponent;
  let fixture: ComponentFixture<PhotographerProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerProfilePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
