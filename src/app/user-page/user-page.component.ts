import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  listItemSelection = 1;
  listClick(event, newValue) {
    this.listItemSelection = newValue;
  }

  constructor() { }

  ngOnInit() {
  }

}
