import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  @Input() category: {"id": number, "categoryName": string};
  @Output() onBackTapped = new EventEmitter();

  // category = {
  //   "id": 1,
  //   "categoryName": "Magazine",
  // }

  isCategoryIdSelected = true;

  constructor() { }

  ngOnInit() {
    console.log(this.category.id);
  }

  backButtonTapped() {
    this.isCategoryIdSelected = false;
    console.log(this.isCategoryIdSelected);
    this.onBackTapped.emit();
  }

}
