import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousBookingsPageComponent } from './previous-bookings-page.component';

describe('PreviousBookingsPageComponent', () => {
  let component: PreviousBookingsPageComponent;
  let fixture: ComponentFixture<PreviousBookingsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousBookingsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousBookingsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
