import { Component, OnInit } from '@angular/core';
import { IMyDpOptions } from '../../../node_modules/angular4-datepicker/src/my-date-picker';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  public myDatePickerOptions: IMyDpOptions = {
      // other options...
      dateFormat: 'dd-mm-yyyy',
  };

  public model: any = { date: { year: 2018, month: 10, day: 9 } };

  constructor() { }

  ngOnInit() {
  }

}
