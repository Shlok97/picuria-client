import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-shortlist',
  templateUrl: './create-shortlist.component.html',
  styleUrls: ['./create-shortlist.component.css']
})
export class CreateShortlistComponent implements OnInit {

  constructor() { }

  isCategorySelected = false;
  categorySelected = 0;
  
  selectCategory(categoryId) {
    this.categorySelected = categoryId;
    this.isCategorySelected = true;
  }
  deselectCategory() {
    this.isCategorySelected = false;
    this.categorySelected = 0;
  }

  ngOnInit() {
  }

}
