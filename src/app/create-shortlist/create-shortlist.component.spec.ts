import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateShortlistComponent } from './create-shortlist.component';

describe('CreateShortlistComponent', () => {
  let component: CreateShortlistComponent;
  let fixture: ComponentFixture<CreateShortlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateShortlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateShortlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
