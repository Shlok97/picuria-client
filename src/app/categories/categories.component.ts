import { Component, ViewChild, OnInit } from '@angular/core';
import { CategoryComponent } from "../category/category.component";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  @ViewChild(CategoryComponent) categorySelectedComponent;

  categoriesData: {id: number, categoryName: string}[] = [
    {
      "id": 0,
      "categoryName": "Wedding",
    },
    {
      "id": 1,
      "categoryName": "Magazine",
    },
    {
      "id": 2,
      "categoryName": "Product",
    },
    {
      "id": 3,
      "categoryName": "Event",
    },
    {
      "id": 4,
      "categoryName": "Personal",
    }
  ];

  constructor() { }
  category: {"id": number, "categoryName": string};
  categoryId = -1;
  isCategoryIdSelected = false;

  categorySelected(id) {
    this.isCategoryIdSelected = true;
    this.categoryId = id;
    this.category = this.categoriesData[id];
  }

  categoryDeselected() {
    this.isCategoryIdSelected = false;
    this.categoryId = -1;
  }

  ngOnInit() {
  }
}
