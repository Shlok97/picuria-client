import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {

  constructor() { }

  isEditing: Array<boolean> = [false, false, false, false];

  ngOnInit() {
  }

  toggleEdit(detailIndex) {
    if(this.isEditing[detailIndex]) {
      this.isEditing[detailIndex] = false;
    }
    else {
      this.isEditing[detailIndex] = true;
    }
    console.log(this.isEditing);
  }

}
