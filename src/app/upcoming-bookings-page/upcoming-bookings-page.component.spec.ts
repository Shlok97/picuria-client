import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingBookingsPageComponent } from './upcoming-bookings-page.component';

describe('UpcomingBookingsPageComponent', () => {
  let component: UpcomingBookingsPageComponent;
  let fixture: ComponentFixture<UpcomingBookingsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpcomingBookingsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingBookingsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
