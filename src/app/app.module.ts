import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MyDatePickerModule } from '../../node_modules/angular4-datepicker/src/my-date-picker';

import { AppComponent } from './app.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './category/category.component';
import { HomeComponent } from './home/home.component';
import { NotificationComponent } from './notification/notification.component';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { RequestsPageComponent } from './requests-page/requests-page.component';
import { UserPageComponent } from './user-page/user-page.component';
import { UpcomingBookingsPageComponent } from './upcoming-bookings-page/upcoming-bookings-page.component';
import { PreviousBookingsPageComponent } from './previous-bookings-page/previous-bookings-page.component';
import { PhotographerProfilePageComponent } from './photographer-profile-page/photographer-profile-page.component';
import { ShortlistPageComponent } from './shortlist-page/shortlist-page.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ChooseShortlistComponent } from './choose-shortlist/choose-shortlist.component';
import { CreateShortlistComponent } from './create-shortlist/create-shortlist.component';


@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    CategoriesComponent,
    CategoryComponent,
    NotificationComponent,
    HomeComponent,
    ProfileComponent,
    SearchPageComponent,
    RequestsPageComponent,
    UserPageComponent,
    UpcomingBookingsPageComponent,
    PreviousBookingsPageComponent,
    PhotographerProfilePageComponent,
    ShortlistPageComponent,
    UpdateProfileComponent,
    ChooseShortlistComponent,
    CreateShortlistComponent
  ],
  imports: [
    BrowserModule,
    MyDatePickerModule,
    NgbModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'notifications', component: NotificationComponent},
      {path: 'search', component: SearchPageComponent},
      {path: 'profile', component: ProfileComponent},
      {path: 'requests', component: RequestsPageComponent},
      {path: 'user', component: UserPageComponent},
      {path: 'photographer', component: PhotographerProfilePageComponent},
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
