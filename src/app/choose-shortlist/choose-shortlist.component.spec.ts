import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseShortlistComponent } from './choose-shortlist.component';

describe('ChooseShortlistComponent', () => {
  let component: ChooseShortlistComponent;
  let fixture: ComponentFixture<ChooseShortlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseShortlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseShortlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
