import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-choose-shortlist',
  templateUrl: './choose-shortlist.component.html',
  styleUrls: ['./choose-shortlist.component.css']
})
export class ChooseShortlistComponent implements OnInit {

  constructor() { }
  isCreatingShortlist = false;

  createShortlist() {
    this.isCreatingShortlist = true;
  }
  
  ngOnInit() {
  }

}
