import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  data = [
    {
      "id": 0,
      "name": "Ram Murti",
      "experience": "Professional",
      "teamSize": 5,
      "rate": 500,
      "pictureUrl": "",
      "portfolioUrl": ""
    },
    {
      "id": 1,
      "name": "Ram Lakshman",
      "experience": "Beginner",
      "teamSize": 1,
      "rate": 200,
      "pictureUrl": "",
      "portfolioUrl": ""
    },
    {
      "id": 2,
      "name": "Lakhan Murti",
      "experience": "Experienced",
      "teamSize": 2,
      "rate": 300,
      "pictureUrl": "",
      "portfolioUrl": ""
    },
    {
      "id": 3,
      "name": "Rajesh Kumar",
      "experience": "Professional",
      "teamSize": 5,
      "rate": 400,
      "pictureUrl": "",
      "portfolioUrl": ""
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
