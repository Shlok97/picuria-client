import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortlistPageComponent } from './shortlist-page.component';

describe('ShortlistPageComponent', () => {
  let component: ShortlistPageComponent;
  let fixture: ComponentFixture<ShortlistPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortlistPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortlistPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
