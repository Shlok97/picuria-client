import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  currentCategory = -1;

  toggleCategory(category) {
    if (this.currentCategory == -1) {
      this.currentCategory = category;
    }
    else {
      this.currentCategory = -1;
    }

    console.log(this.currentCategory);
  }

  ngOnInit() {
  }

  redirect() {
    this.router.navigate(['./search']);
  }

}
